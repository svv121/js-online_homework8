"use strict";
/*
1. DOM - це відображення HTML-документа у вигляді дерева об'єктів. Це дерево формується за рахунок вкладеної структури окремих вузлів - тегів і текстових елементів.
2. innerText відображатиме значення як є у вигляді рядка, і відображатиме елемент HTML на екрані як HTML-код і ігнорує будь-яке форматування HTML, яке може бути включено.
innerHTML покаже значення, текстовий вміст і застосуватиме форматування HTML.
3. Об'єкт DOM можна отримати за допомогою методів: document.getElementById(), document.getElementsByName(), document.getElementsByTagName(), document.getElementsByClassName(), document.querySelector(), document.querySelectorAll(). Найкращий - той, що найбільше підходить для вирішення конкретного завдання. Найчастіше використовуються методи document.querySelector(), document.querySelectorAll().
*/
const pElements = [...document.getElementsByTagName('p')];
pElements.forEach((element) => {
    element.style.background = '#ff0000';
    element.style.opacity = '1';
});
const optionsList = document.getElementById('optionsList');
console.log('________id_optionsList:_________');
console.log(optionsList);
console.log('________parentElement_of_optionsList:_________');
let optionsChildNodes
if (!!optionsList) {
    console.log(optionsList.parentElement);
    optionsChildNodes = [...optionsList.childNodes];
}
else{optionsChildNodes = []}
function showArrayElements(someArray) {
    if(!!someArray) {
        someArray.forEach(element => console.log(element));
    }
}
console.log('________childNodes_of_optionsList:_________');
showArrayElements(optionsChildNodes);

console.log('________childNodes_types:_________');
optionsChildNodes.forEach(elem=>console.log(elem.nodeType));
console.log('________childNodes_names:_________');
optionsChildNodes.forEach(elem=>console.log(elem.nodeName));

//in index.html there is the id 'testParagraph', not the class 'testParagraph'
const testParagraph = document.querySelector('#testParagraph');
if (!!testParagraph) {
    testParagraph.textContent = 'This is a paragraph';
}

const mainHeader = [...document.querySelector('.main-header').children];
console.log('________.main-header_children:_________');
mainHeader.forEach(elem => {
console.log(elem);
elem.classList.add('nav-item');
});

//there is no elements with the class 'section-title' in index.html, I used another class instead
const productsListItem = document.querySelectorAll('.products-list-item');
productsListItem.forEach((elem) => {
    elem.classList.remove('products-list-item')
});